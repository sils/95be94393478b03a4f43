diff --git a/coalib/coala_json.py b/coalib/coala_json.py
index 1b2d7f2..a660b18 100644
--- a/coalib/coala_json.py
+++ b/coalib/coala_json.py
@@ -13,7 +13,7 @@
 
 import json
 
-from coalib.output.printers.ListLogPrinter import ListLogPrinter
+from coalib.output.printers.ConsolePrinter import ConsolePrinter
 from coalib.processes.Processing import execute_section
 from coalib.settings.ConfigurationGathering import gather_configuration
 from coalib.results.HiddenResult import HiddenResult
@@ -23,7 +23,7 @@ from coalib.output.Interactions import fail_acquire_settings
 
 
 def main():
-    log_printer = ListLogPrinter()
+    log_printer = ConsolePrinter()
     exitcode = 0
     results = {}
     try:
@@ -63,12 +63,12 @@ def main():
     except Exception as exception:  # pylint: disable=broad-except
         exitcode = exitcode or get_exitcode(exception, log_printer)
 
-    retval = {"logs": log_printer.logs, "results": results}
-    retval = json.dumps(retval,
+    retval = json.dumps(results,
                         cls=JSONEncoder,
                         sort_keys=True,
                         indent=2,
                         separators=(',', ': '))
-    print(retval)
+    with open("output.json", "w") as fi:
+        fi.write(retval)
 
     return exitcode
